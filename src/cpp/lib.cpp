//lib.cpp
// #include <stdio.h>
// #include <iostream>
#include <stdint.h>
#include <emscripten.h>

// By default, the functions don't get exported expect for main
// so we add extern "C" to  avoid C++ name mangling
// and EMSCRIPTEN_KEEPALIVE to this purpose
extern "C" {
  /*
  EMSCRIPTEN_KEEPALIVE
  void read_raw(unsigned int width, unsigned int height, unsigned int length, uint8_t* tab) {
    std::cout << "Width: " << width << " Height: " << height << " Length: " << length << std::endl;
    
    for(uint8_t i = 0; i < 20; ++i) {
        printf("%d ", *(tab + i));
    }

    std::cout << std::endl;
  }
*/
  EMSCRIPTEN_KEEPALIVE
  void invert_raw(unsigned int width, unsigned int height, unsigned int length, uint8_t* tab) {
    for(unsigned int i = 0; i < length; i+=3) {
        *(tab + i) = 255 - *(tab + i);
        *(tab + i + 1) = 255 - *(tab + i + 1);
        *(tab + i + 2) = 255 - *(tab + i + 2);
    }
  }
}
