# Content

This folder contains `.wasm`, `.js` glue code and a wrapper to work with. As well a C++ sources.

`doemcc.sh` contains the build command I used. Of course it should be changed to a more mature way if needed for a production project. 

## How to WASM

### Install

Clone the repo with the last version somewhere

```bash
git clone https://github.com/emscripten-core/emsdk.git
cd emsdk
```

Install the sdk

```bash
./emsdk install latest
./emsdk activate latest
source ./emsdk_env.sh
```

And then compile it with:

```bash
emcc hello.c -o hello.js
```

and test your code:

```bash
node hello.js
```

