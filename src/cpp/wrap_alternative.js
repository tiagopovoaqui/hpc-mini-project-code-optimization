var em_module = require('./lib.js');

em_module['onRuntimeInitialized'] = onRuntimeInitialized;

function onRuntimeInitialized() {
    // em_module._read_raw(); // direct calling works
    em_module.ccall("read_raw", 'null', ['number', 'number', 'Uint8Array'], [12, 13, null]); // name, return type, args
    em_module.cwrap("read_raw", 'null', ['number', 'number', 'Uint8Array']);
    module.exports.func = onRuntimeInitialized;
}
