var CPP_LIB = require('./lib.js');

let _invert_raw;

const lib_promise = CPP_LIB().then((Module) => {
    if(typeof _invert_raw === 'undefined') {
        _invert_raw = Module.cwrap("invert_raw", 'null', ['number', 'number','number', 'Uint8Array']);
    }

    return { Module, _invert_raw };
});

async function invert_raw(width, height, data) {
    const { Module, _invert_raw } = await lib_promise;

    const buf = Module._malloc(data.length*data.BYTES_PER_ELEMENT);
    Module.HEAPU8.set(data, buf);

    _invert_raw(width, height, data.length, buf);  

    var output_array = new Uint8Array(Module.HEAPU8.buffer, buf, data.length);

    Module._free(buf);

    return output_array;
}

module.exports = { invert_raw };