/**
 * Add a promise with the method you wish to test and resolve it in the correct manner.
 * You'll be able to test the perfs in index.js
 */
const Jimp = require('jimp');
const { invert } = require('./experiment.js');

// Files configuration
const folder_in = 'img/'
const folder_out = 'out/'
const image_name = 'erminea_2.jpg'

/**
 * Use this utile function to prepare a promise to measure
 * Not really a factory. Good enough for my test :)
 * @param {string} choice 
 */
function promiseFactory(choice) {
    switch(choice) {
        case 'jimp_invert':
            return new Promise((resolve, reject) => {
                Jimp.read(`${folder_in}${image_name}`, (err, _image) => {
                    if (err) throw err;
                    _image
                        .invert() // set greyscale
                        .write(`${folder_out}jimp_invert_${image_name}`); // save
            
                        resolve();
                });
            });

        case 'cpp_invert':
            return new Promise((resolve, reject) => {
                invert(folder_in, folder_out, image_name)
                .then(() => {
                    resolve();
                });
            });
    }
}

module.exports = { promiseFactory };