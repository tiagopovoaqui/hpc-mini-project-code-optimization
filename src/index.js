const {
    performance,
    PerformanceObserver
} = require('perf_hooks');

const { promiseFactory } = require('./promise_factory.js')

const obs = new PerformanceObserver((items) => {
    console.log(items.getEntries()[0].name, items.getEntries()[0].duration, 'ms (time elapsed)');
    performance.clearMarks();
});

obs.observe({ entryTypes: ['measure'] });

function measure(markA, markB, promise) {
    performance.mark(markA);
    promise.then(() => {
        performance.mark(markB);
        performance.measure(`${markA}_ to _${markB}`, markA, markB);
    });
}

// Call any method you wish to measure
// Just add the promise wrapper to the factory, or insert it directly here
// measure('J invert A', 'J invert B', promiseFactory('jimp_invert'));
measure('Cpp invert A', 'Cpp invert B', promiseFactory('cpp_invert'));

