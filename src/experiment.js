const fs = require('fs');
const jpeg = require('jpeg-js');
const { invert_raw } = require('./cpp/wrap.js');

async function invert(folder_in, folder_out, image_name) {
  const jpegData = fs.readFileSync(`${folder_in}${image_name}`);
  const rawImageData = jpeg.decode(jpegData, {useTArray: true}); // return as Uint8Array

  const output = await invert_raw(rawImageData.width,rawImageData.height, rawImageData.data);

  rawImageData.data = output;
  const jpegImageData = jpeg.encode(rawImageData, 50);

  fs.writeFileSync(`${folder_out}cpp_invert_${image_name}`, jpegImageData.data);
}

module.exports = { invert };