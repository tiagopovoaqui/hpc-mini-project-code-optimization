# HPC Project one: code optimization

> Tiago Povoa Quinteiro - HEIG-VD - Course: High Performance Coding

---

**Assignement:**

> Details: you have to choose one open source software; you have to install it on your machine, benchmark it and develop a performance improvement (memory, speed, or any other important metric for the chosen software, latency for instance).

## Introduction

For my project, I proposed to work on **Jimp**. It is an image processing library (MIT License). Jimp, as is, is a good and lightweight Javascript library.  

Since HPC is a course about low level optimizations, I thought it would be interesting to have a look at **Web Assembly.** It allows us to run code, written traditionally to target native platforms,  on the browser or Node.js, alongside Javascript.

From the front page https://webassembly.org/ : 

> WebAssembly (abbreviated *Wasm*) is a binary instruction format for a stack-based virtual machine. Wasm is designed as a portable target for compilation of high-level languages like C/C++/Rust, enabling deployment on the web for client and server applications.

## Running the code

Entrypoint is index.js

```
npm install
npm start
```

## Installing the toolchain

> Note: If you need to compile the C++ sources yourself

1. Clone the repo with the last version

```bash
git clone https://github.com/emscripten-core/emsdk.git
cd emsdk
```

2. Install the sdk

```bash
./emsdk install latest
./emsdk activate latest
source ./emsdk_env.sh
```

---

My version of **emcc** was: **1.39.16**. To run the code you'll also need **Node.js** installed. I worked with **v14.3.0**, but the current LTS might work as well.

### Testing on a hello world example

Write some basic hello world in c in a main function.

1. compile it with:

```bash
emcc hello.c -o hello.js
```

2. test your code with node

```bash
node hello.js
```

For a more detailed installation guide: https://emscripten.org/docs/getting_started/Tutorial.html

## Porting to C/C++

> If you are not interested in this aspect, skip this section
>
> I added a lot of technical details since it took me a lot of time to understand

### Initial code (JS)

Before we talk about optimizations, we have to port some code to C++.

For example, let's say we want to invert some picture. With Jimp we would simply do:

> I took Invert as an example since it is a simple algorithm.

```js
const folder_in = 'img/'
const folder_out = 'out/'
const image_name = 'erminea_2.jpg'

const Jimp = require('jimp');

Jimp.read(`${folder_in}${image_name}`, (err, _image) => {
  if (err) throw err;
  _image
    .invert() // here the magic happens, inverting the image.
    .write(`${folder_out}invert_${image_name}`); // save
});
```

Some neat Javascript with asynchronous callbacks. 

Here the provided picture is a `jpeg` file. So, before processing it, there is a required step to transform it to a `raw` format (RGB or RGBA).

Then, to invert the colors, it has to iterate over the components of the pixels. Really nothing fancy. I choose a simple example on purpose because I'm still a `wasm` beginner.

```js
invert(cb) {
    this.scanQuiet(0, 0, this.bitmap.width, this.bitmap.height, function(
      x,y,idx
    ) {
      this.bitmap.data[idx] = 255 - this.bitmap.data[idx];
      this.bitmap.data[idx + 1] = 255 - this.bitmap.data[idx + 1];
      this.bitmap.data[idx + 2] = 255 - this.bitmap.data[idx + 2];
    });
```

source: https://github.com/oliver-moran/jimp/blob/master/packages/plugin-invert/src/index.js

And obviously, it has to read (before) and write the file (after).

---

### Writing C++

Now let's talk about the C++ version:

```cpp
#include <emscripten.h>

extern "C" {
  EMSCRIPTEN_KEEPALIVE
  void invert_raw(unsigned int width, unsigned int height, unsigned int length, uint8_t* tab) {
    for(unsigned int i = 0; i < length; ++i) {
        *(tab + i) = 255 - *(tab + i);
    }
  }
}
```

Except for the `main()`, by default, functions don't get exported. So we have to annotate with `EMSCRIPTEN_KEEPALIVE` (We could also manually specify the functions to export in the compiler options like this `-s EXPORTED_FUNCTIONS='["_invert_raw"]'`.)

Then, we have to add `extern "C"` to  avoid C++ name mangling. (The functions in C++ don't use the name as a unique id since we can overload them anyway).

---

### Compiling to wasm

> Note: here I use the official example code.

We could simply do the following to run our code.

`emcc api_example.cpp -o api_example.js`

However, we don't have a main function. Otherwise we could just call a generated `.js` file and test it directly. But it wouldn't be very useful. 

Instead, we want to import it as a module and use it as a function from a library. We can do that in three ways:

```js
var em_module = require('./api_example.js');

em_module._sayHi(); // direct calling works
em_module.ccall("sayHi"); // using ccall etc. also work
console.log(em_module._daysInWeek()); // values can be returned, etc.
```

*source:* https://emscripten.org/docs/porting/connecting_cpp_and_javascript/Interacting-with-code.html#interacting-with-an-api-written-in-c-c-from-nodejs

As we compile the C++ file, emcc makes two files for us: a `wasm` and a `js`. The javascript one prepares for us a runtime to make it easier to work with wasm.

But the code above doesn't work as is. First, it would assert an error like: `you need to wait for the runtime to be ready (e.g. wait for main() to be called)`. And then, an other error because `ccall` here would be undefined. For this one, just add the following compiler option: `-s EXTRA_EXPORTED_RUNTIME_METHODS='["ccall"]'`

There is two solutions:

a) wrap everything like that:

```js
var em_module = require('./api_example.js');

em_module['onRuntimeInitialized'] = onRuntimeInitialized;

function onRuntimeInitialized() {
    em_module._sayHi(); // direct calling works
    em_module.ccall("sayHi"); // using ccall etc. also work
    console.log(em_module._daysInWeek()); // values can be returned, etc.
}
```

The example above allows the runtime to be ready. But isn't very practical to export the functions we just defined.

b) modularize it:

```js
em_module().then((Module) => {
 	em_module._sayHi(); // direct calling works
	// You could wrap a function with cwrap and return it from here.
    return { Module, ... };
});
```

By adding two compilation options, we can use `.then()` to execute a proper callback when the runtime is ready. For this we have to add `emcc api_example.cpp -o api_example.js MODULARIZE=1 -s EXPORT_NAME=em_module`. 

I added everything I mentioned above to this report since it caused me a lot of trouble. It might help someone else. I found my answers thanks to the nice community on the wasm discord server. 

### First version

we already discussed about the c++ code. It is as simple as possible. We went through everything we need to use it in Javascript. 

So here is the function we provide: 

```js
async function invert(folder_in, folder_out, image_name) {
  const jpegData = fs.readFileSync(`${folder_in}${image_name}`);
  const rawImageData = jpeg.decode(jpegData, {useTArray: true}); // return as Uint8Array

  const output = await invert_raw(rawImageData.width,rawImageData.height, rawImageData.data);

  rawImageData.data = output;

  const jpegImageData = jpeg.encode(rawImageData, 50);

  fs.writeFileSync(`${folder_out}cpp_invert_${image_name}`, jpegImageData.data);
}
```

I took a look into the library to see what kind of jpeg conversion they used. Here I'm using **jpeg-js**. 

*source* https://www.npmjs.com/package/jpeg-js

As a first version, there is a lot of stuff not optimal here. We read files synchronously (but we shouldn't use blocking IO in JS). 

As mentioned in the npm package:

> **NOTE:** this is a *synchronous* (i.e. CPU-blocking) library that is much slower than native alternatives. If you don't need a *pure javascript* implementation, consider using async alternatives like [sharp](http://npmjs.com/package/sharp) in node or the [Canvas API](https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API) in the browser.

Also, if we look into the wrapping function `invert_raw`:

```js
async function invert_raw(width, height, data) {
    const { Module, _invert_raw } = await lib_promise;

    const buf = Module._malloc(data.length*data.BYTES_PER_ELEMENT);
    Module.HEAPU8.set(data, buf);

    _invert_raw(width, height, data.length, buf);  

    var output_array = new Uint8Array(Module.HEAPU8.buffer, buf, data.length);

    Module._free(buf);

    return output_array;
}
```

In order to pass data to C/C++ code, we have to prepare it manually. Here we allocate with `_malloc` and set this memory area as uint8 in the heap: `HEAPU8.set()`. Then, to get the data back, we have to re-transform it as a type Javascript can understand: `Uint8Array`. And of course `_free` the memory ;).

A lot of overhead it seems, right? When I was preparing this testing code, I was even wondering if it could make an improvement over the original one. 

We also have to discuss some limitations. On the stack, we have a limit around  **5 MB**, on the heap the default should be **16 MB**. If we need to grow it further, we have to add the following compile flag `-s ALLOW_MEMORY_GROWTH=1`. Then, we would only be limited by Node or the browser (~1GB). 

*source:* https://marcoselvatici.github.io/WASM_tutorial/#memory

## Benchmarking

### Measure protocol

To measure performance, I'll use the **Performance Timing API**  from Node.js. Since most calls are async in Javascript, I'll just wrap the tested code inside a promise. When eventually the promise resolves, we measure the elapsed time. We cannot run two tests at the same time since asynchronous tasks might be mixed up during execution, time would not be relevant.

*source:* https://nodejs.org/api/perf_hooks.html

* The image we test is a jpg of 2.7 MB, 3872x2592.

* We make 5 measurements each time and take max and min. 
* In case I observe some extreme values (a more sided distribution), I may also do a mean.

* Tested on a Intel i7-4770K

## Optimizations

### Compiler optimizations levels

The first thing we can look into is optimization flags.

By just tweaking the level of optimization I got already interesting results: 

|         | Jimp       | -O0    | -O1    | -O2    | -O3    | -Os    | -Oz    |
| ------- | ---------- | ------ | ------ | ------ | ------ | ------ | ------ |
| **max** | 3469.79 ms | 983.74 | 638.32 | 646.58 | 635.22 | 625.99 | 620.38 |
| **min** | 3403.35    | 653.14 | 625.04 | 615.61 | 614.59 | 598.59 | 610.54 |

The regular levels are `-O0` to `-O3`. But we can also choose `-Os` and `-Oz`. They try to focus on reducing the code size. The second one doing more trade-offs towards size.

We can also add `--closure 1` to reduce the generated javascript glue code. In my case, there was no real benefit observed. 

> Note, this optimization is marked as unsafe in the documentation. Further reading is needed if you want to use it.

We can also reduce C++ run-time type info and exceptions with `-fno-rtti -fno-exceptions` in cases they are not needed. It shrinks the output. I saw little to no difference with `-Os` but a bit of improvement with `-O3`. 

### Manual optimization

I tried some simple loop unrolling. 

```cpp
void invert_raw(unsigned int width, unsigned int height, unsigned int length, uint8_t* tab) {
    for(unsigned int i = 0; i < length; i+=3) {
        *(tab + i) = 255 - *(tab + i);
        *(tab + i + 1) = 255 - *(tab + i + 1);
        *(tab + i + 2) = 255 - *(tab + i + 2);
    }
}
```

This way we loop over the three components at the same time.

I couldn't test this optimization at `-O3` or `-Os` because of this strange bug `TypeError: Module._malloc is not a function`. Even adding manually `-s EXTRA_EXPORTED_RUNTIME_METHODS='["_malloc"]'` didn't fix the issue.

In spite of this small issue, I still was able to test it with `-O2` which gave me a nice improvement:

**min**: 566.94 ms, **max**: 582.14 ms

## Go further

### SIMD

I was very curious about SIMD in wasm. I tried to enable auto-vectorization with the following compiler options:`-msimd128 -s SIMD=1`, but it didn't work. I had some strange and unknown error. After a couple hours, I just gave up.

What I can tell is that you'd need to add the following flag to Node: `--experimental-wasm-simd`

You could also use the specific wasm simd operations with this header `#include <wasm_simd128.h>`.

Currently, x86 true SIMD intrinsics don't work directly. But it seems they could be translated automatically to wasm. 

I was a bit disappointed not to test this feature. It has a strong potential.

### Better jpg conversion

In our current code, we use jpeg-js, which is great but we could do something to decode/encode our jpeg faster. 

Imagine everything we could do by accessing well known libraries from the C/C++ world like ImageMagick, OpenCV, CImg, ... We might just have some trouble to compile/link one of those. But the improvements could be interesting. 

### Buffers

Currently we read everything in a whole huge chunk. It could be improved by better using asynchronous operations instead of blocking ones. We would definitely have to do that in production to avoid taking too large portions of memory.

Another note, we could improve the read operations by doing them directly in wasm. But I didn't go too deep in this subject since it has currently some limitations due to how those things are handled in browsers.

## Conclusion

This project was a very interesting and satisfying, yet challenging, experience. Getting the hands on emcc was difficult. The documentation was mostly okay, but the lack of posts about this subject and the rapid changes make it hard to get into with no experience at all.

A very trivial porting made already a tremendous change in speed. At the cost of having to build your code beforehand of course. Going this route opens the door to more optimization low level programmers are familiar with. But as also some limitations with file system, simd, pthreads (since Javascript only has web workers and not posix threads).

Finally, is it worth going through so much trouble for this particular example? Probably not. There is two goods alternatives:

1. Node: Sharp based on libvips

   *source:* https://sharp.pixelplumbing.com/

   https://github.com/libvips/libvips

2. Browser: you can use directly the Canvas API from the browser.

However, I hope this experiment showed an accessible view of the potential of `wasm`.  

*Final note: I've encountered some problems with the official documentation during the porting phase. I added an issue on github and am happy to see that the problem was fixed very recently.* :)

## Sources/ Links

https://webassembly.org/docs/c-and-c++/

https://emscripten.org/docs/getting_started/Tutorial.html

https://wasmbyexample.dev/examples/hello-world/hello-world.c.en-us.html#